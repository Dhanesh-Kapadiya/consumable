import React from "react"
import ReactDOM, { render } from "react-dom"

import App from "./containers/App.jsx"

ReactDOM.render(<App />, document.getElementById("app"))