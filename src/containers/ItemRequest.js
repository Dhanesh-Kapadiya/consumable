import React from "react"

export default class ItemRequest extends React.Component {
	render() {
		return(
			<div className="item-request">
				<div className="profile-pic">
				</div>

				<div className="profile-name">
				</div>

				<div className="item-detail">
				</div>

				<div className="item-price-range">
				</div>

				<button className="connect-btn">
					Connect
				</button>
			</div>
		)
	}
}