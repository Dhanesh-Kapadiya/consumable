**1.** Create and Run MongoDB.

   1.1 create a DB with name **item-request**

   1.2 create a collection with name **allRequests**

   1.3 And run MongoDb

**2.** Clone this repo and Install dependencies

   git clone https://Dhanesh-Kapadiya@bitbucket.org/Dhanesh-Kapadiya/consumable.git

   cd consumable

   npm install

**3.** Run express server in a terminal

   npm run server

**4.** Run webpack dev server in a separate terminal

   npm start
   
**5.** open **http://localhost:8089/**